import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {
	
	/*
	 * Returns value of specified key from config.properties file
	 * 
	 * @param key - key of respective value requested
	 * @return - value of respective key/value pair
	 */
	public static String getPropValue(String key) {
		String value = "";
		InputStream inputStream = null;
		
		try {
			Properties prop = new Properties();
			String propFileName = "config.properties";
			
			inputStream = ConfigReader.class.getClassLoader().getResourceAsStream(propFileName);
			
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file: " + propFileName + " not found");
			}
			
			// Get property value
			value = prop.getProperty(key); 
			
		} catch (Exception e) {
			System.err.println("ConfigReader.getPropValues() ERROR:");
			e.printStackTrace();
		} finally {
			try { inputStream.close(); } catch (IOException e) { e.printStackTrace(); }
		}
		
		return value;
	}
}
