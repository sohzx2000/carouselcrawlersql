import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Tool {
	
	public static void main(String args[]) {
		getCollectionIDs();
	}
	
	/*
	 * TEMPORARY: program to crawl all collection IDs from carousell
	 */
	public static void getCollectionIDs() {
		String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";
		List<String> collections = new LinkedList<String>();
	
		try {
			Connection connection = Jsoup.connect("https://sg.carousell.com/").userAgent(USER_AGENT);
			Document htmlDocument = connection.get();
			
			if (connection.response().statusCode() == 200) {
				// HTTP OK
				System.out.println("\n**Visiting** Received web page at " + "https://sg.carousell.com/");
			}
			
			if (!connection.response().contentType().contains("text/html")) {
				// page is not HTML
				System.out.println("**Failure** Retrieved something other than HTML");
			}
			
			// collect all the links [modified to get next page links]
			Elements linksOnPage = htmlDocument.select("a.MenuItem__menuItemLink___1jLty");
			for (Element link : linksOnPage) {
				collections.add(link.absUrl("href"));
			}
		} catch (IOException e) {
			// unsuccessful in our HTTP request
			System.out.println("Error in out HTTP request" + e);
		}
		
		// Identify sub-categories
		String baseCollection = "";
		for (String collection : collections) {
			if (!collection.contains(baseCollection)) {
				System.out.println();
			}
			String[] parts = collection.split("/");
			baseCollection = parts[4]; 
			String curCollection = parts[parts.length - 1];
			String curCollectionName = curCollection.substring(0, curCollection.lastIndexOf('-'));
			String curCollectionID = curCollection.substring(curCollection.lastIndexOf('-') + 1);
			System.out.println(curCollectionName + ": " + curCollectionID);
		}
	}
	
	/*
	 * Converts age in String to seconds
	 * 
	 * @param age - String value of product/target age
	 * @return - value of age in seconds
	 */
	public static int ageToSeconds(String age) {
		int seconds = 0;
		
		if (age.contains("second")) {
			seconds = timeMagnitude(age);
		} else if (age.contains("minute")) {
			seconds = timeMagnitude(age) * 60;
		} else if (age.contains("hour")) {
			seconds = timeMagnitude(age) * 3600;
		} else if (age.contains("yesterday")) {
			seconds = 86400;
		} else if (age.contains("day")) {
			seconds = timeMagnitude(age) * 86400;
		} else if (age.contains("last month")) {
			seconds = 30 * 86400;
		} else if (age.contains("month")) {
			seconds = timeMagnitude(age) * 30 * 86400;
		} else if (age.contains("last year")) {
			seconds = 12 * 30 * 86400;
		} else if (age.contains("year")) {
			seconds = timeMagnitude(age) * 12 * 30 * 86400;
		}
		return seconds;
	}
	
	/*
	 * returns the magnitude of a String time unit 
	 * 
	 * @param - string to get magnitude from
	 * @return - magnitude of time as an integer
	 */
	private static int timeMagnitude(String age) {
		return Integer.parseInt(age.split("\\s+")[0]);
	}
	
	/*
	 * Deletes all txt files in selected folder
	 * 
	 * @param folderLocation - location of folder to delete
	 */
	public static void deleteFiles(String folderLocation) {
		System.out.println("Tools.deleteFiles() running ...");
		File folder = new File(folderLocation);
	
	    File[] files = folder.listFiles();
	    if(files!=null) { //some JVMs return null for empty dirs
	        for(File f: files) {
	        	System.out.println("Attempting to delete:" + f);
	        	if (f.delete()) {
	        		System.out.println("Successfully deleted");
	        	} else {
	        		System.out.println("Failed to delete");
	        	}
	        }
	    }
	}
	
	/* 
	 * Returns result of Y/N questions 
	 * 
	 * @param prompt - question to prompt
	 */
	public static boolean isTrue(String prompt) {
		Scanner scn = new Scanner(System.in);
		char response;
		boolean result;
		
		while (true) {
			System.out.print(prompt + " [Y/N]: ");
			response = scn.nextLine().charAt(0); /* only concerned with first character, Y/y/N/n */
			
			if (response == 'Y' || response == 'y') {
				result = true;
				break;
			} else if (response == 'N' || response == 'n') {
				result = false;
				break;
			} else {
				System.out.println("Sorry, please try again, enter 'Y' or 'N' only\n");
			}
		}
		return result;
	}
	
}
