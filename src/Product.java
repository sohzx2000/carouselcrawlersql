
public class Product implements Comparable<Product>{

	// Attributes
	private String name;
	private double price;
	private String link;
	private String seller;
	private String age;
	private int ageInSeconds;
	
	/*
	 * Constructor
	 */
	public Product(String name, String price, String link, String seller, String age) {
		this.name = name;
		this.price = Double.parseDouble(price.replaceAll("[^\\d.]",  ""));
		if (link.indexOf('?') > 0) {
			this.link = link.substring(0, link.indexOf('?') - 1); // remove unnecessary query parameters from link
		} else {
			this.link = link;
		}
		this.seller = seller;
		this.age = age;
		this.ageInSeconds = Tool.ageToSeconds(age);
	}
	
	/*
	 * Formats attributes into readable string
	 * 
	 * @param includeLink - choose if link should be printed
	 */
	public String toString(boolean includeLink) {
		String format;
		format = "[" + this.name + "] " + " $" + this.price;
		
		if (includeLink) {
			format += "\n" + this.link;
		}
		
		format += "\nSold by: " + this.seller
			   + "\nAge: " + this.age;
		
		return format;
	}
	
	public String getName() {
		return this.name;
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public String getSeller() {
		return this.seller;
	}
	
	public String getLink() {
		return this.link;
	}
	
	public String getAge() {
		return this.age;
	}
	 
	public int getAgeInSeconds() {
		return this.ageInSeconds;
	}

	@Override
	public int compareTo(Product product) {
		int result = 0;
		if (this.price > product.price) {
			result = 1;
		} else if (this.price < product.price) {
			result = -1;
		} else {
			result = 0;
		}
		return result;
	}
}
