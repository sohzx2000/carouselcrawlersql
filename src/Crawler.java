import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import javax.mail.MessagingException;

public class Crawler extends TimerTask{
	
	private int userID;
	private List<Integer> groupIDs = null;
	public static final String[] ignoreList = {"https://sg.carousell.com/p/"};
	private static final String EMAIL = ConfigReader.getPropValue("maileruser");
	private static  final String PASSWORD = ConfigReader.getPropValue("mailerpass");
	
	/* Used to set crawl type, 'recent posts' or 'all relevant posts' */
	public static enum CRAWLTYPE {SUMMARY, ALERT};
	private Crawler.CRAWLTYPE crawlType;
	private int alertInterval;
	
	/*
	 * Constructor SUMMARY mode
	 */
	public Crawler(int userID, Crawler.CRAWLTYPE crawlType) {
		this.crawlType = crawlType;
		this.userID = userID;
		this.alertInterval = 0;
	}
	
	/*
	 * Constructor ALERT mode
	 */
	public Crawler(int userID, Crawler.CRAWLTYPE crawlType, int alertInterval) {
		this.crawlType = crawlType;
		this.userID = userID;
		this.alertInterval = alertInterval;
	}
	
	/*
	 * Creates a spider (which creates spider legs) and crawls the web 
	 * Results are stored in mySQL database (local/AWS RDS)
	 * Results are also delivered via email
	 */
	@Override
	public void run() {	
		Calendar calendar = Calendar.getInstance();
		System.out.println("Scheduled run [" + this.crawlType + "] started at: " + calendar.getTime());
		
		// get id of all user's groups to crawl
		//if (this.groupIDs == null) { 
			groupIDs = SQLManager.getGroupIDs(this.userID);
			System.out.println("Categories found: " + groupIDs.size());
		//}
		
		/* List of all the spider threads that are running */
		List<Thread> threads = new LinkedList<Thread>();
		
		// retrieve targets
		for (int groupID : groupIDs) {
			List<Target> targets = SQLManager.getTargets(groupID);
			
			for (Target target : targets) {
				System.out.println("\n=== CURRENT OPERATION ===" + "\n" + target.getName());	
				
				// commence search
				Spider spider = new Spider(target, crawlType, alertInterval);
				Thread spiderThread = new Thread(spider);
				threads.add(spiderThread);
				spiderThread.start();
			}
		}
		
		// wait for all spider threads to complete before going beyond this comment
		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				// this is the main thread, nothing should cause a interrupt here
				e.printStackTrace();
			}
		}
		
		// e-mail results on a per group basis
		for (int groupID : groupIDs) {
			mailResults(groupID);
		}
		
		//reset results (is_active = false) in DB
		SQLManager.resetResults(groupIDs);
	}
	
	/*
	 * Sets up mailer and mails results
	 * email user and password currently hard-coded
	 * 
	 * @param groupID - id of current group to email
	 * @return - true if mail sent successfully, false otherwise
	 */
	private boolean mailResults(int groupID) {
		String groupName = SQLManager.getGroupName(groupID);
		System.out.println("\n=== Preparing to send mail for:" + groupName + " ===");
		
		// set up Mailer
		Mailer mailer = null;
		try {
			mailer = new Mailer(EMAIL, PASSWORD);
		} catch (MessagingException e) {
			System.err.println("mailResults() ERROR: failed to set up mailer");
			e.printStackTrace();
			return false;
		}
		
		// generate contents of mail
		String contents = "";
		contents = generateHTML(groupID);
		if (crawlType == CRAWLTYPE.ALERT && contents.isEmpty()) {
			System.out.println("Nothing to send for: " + groupName + ", operation " + crawlType);
			return false;
		}
		
		// send email
		String userEmail = SQLManager.getUserEmail(userID);
		String title = "";
		if (crawlType == CRAWLTYPE.SUMMARY) {
			title = "[ " + groupName + " ] summary";
		} else if (crawlType == CRAWLTYPE.ALERT) {
			title = "-- new matches --  [" + groupName + "]";
		}
		
		try {
			mailer.sendEmail(userEmail , title , contents);
		} catch (MessagingException e) {
			System.err.println("mailResults() ERROR: failed to send email");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/*
	 * Generates HTML from all results within a results folder
	 * 
	 * @param groupID - ID to query results from
	 * @return - contents in HTML format
	 */
	private String generateHTML(int groupID) {
		System.out.println("Generating email contents...");
		String contents = "";
		
		// get list of targets in current group from targets table
		List<Target> targets = SQLManager.getTargets(groupID);
		
		// get results for each target from results table as List of products
		for (Target target : targets) {
			List<Product> results = SQLManager.getActiveResults(target.getID());
			
			// CRAWLTYPE.NEW does not list empty results
			if (this.crawlType == CRAWLTYPE.ALERT) {
				if (results.size() == 0) { continue; };
			}
			
			double opacity = 1;				//tracks opacity product div
			double opacityChange = 0.025;	//sets opacity change rate

			// <div> for header
			contents += "<div style=\"background-color:rgba(248, 29, 18, 0.62); "
					 + "font-weight:bold; "
					 + "margin-top: 10px; "
					 + "margin-bottom: 10px; "
					 + "padding:10px; "
					 + "border-radius:5px;\">";
			
			contents += "<p>" + "=== " + target.getName() + " ===" + "</p><br>";
			contents += "<p>" + "=== " + results.size() + " product(s) found ===" + "</p><br>";
			contents += "</div>";
			
			// opening <div> tag for results wrapper
			contents += "<div style=\"background-color:rgba(115, 115, 115, 0.3); "
					 + "padding:10px; "
					 + "border-radius:5px;\">";
			
			for (Product result : results) {
				// opening <div> tag for result
				contents += "<div style=\"background-color:rgba(255, 255, 255," + opacity + "); "
							 + "margin-bottom: 10px; "
							 + "padding:5px; "
							 + "border-radius:5px;\">";
				opacity -= opacityChange;
				
				// result contents
				contents += "<p style=\"font-weight:bold;\">" + "[" + result.getName() + "]  $" + result.getPrice() + "</p>";				
				contents += "<p>" + result.getLink() + "</p>";
				contents += "<p>" + "Sold by: " + result.getSeller() + "</p>";
				contents += "<p>" + "Age: " + result.getAge() + "</p>";
				
				// closing </div> tag for result
				contents += "</div>";
			}
			
			// closing </div> tag for results wrapper
			contents += "</div>";
		}
		return contents;
	}
	
}
