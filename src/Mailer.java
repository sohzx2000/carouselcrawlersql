import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 
public class Mailer {
 
	Properties mailServerProperties;
	Session mailSession;
	MimeMessage mailMessage;
	Transport transport;
	String email;
	String password;
 
	/*
	 * Constructor
	 * 
	 * @param email - email of sender account
	 * @param password - password of sender account
	 */
	public Mailer(String email, String password) throws AddressException, MessagingException {
		this.email = email;
		this.password = password;
		
		// Step1
		System.out.println("\nSetting up Mail Server Properties..");
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", "587");
		mailServerProperties.put("mail.smtp.auth", "true");
		mailServerProperties.put("mail.smtp.starttls.enable", "true");
		System.out.println("Mail Server Properties have been setup successfully..");
		 
		// Step2
		System.out.println("\ngetting Mail Session..");
		mailSession = Session.getDefaultInstance(mailServerProperties, null);
		System.out.println("Mail Session has been created successfully..");
	}
 
	/*
	 * Sends E-mail
	 * 
	 * @param recipient - email of results recipient
	 * @param subject - subject of email
	 * @param contents - contents of email
	 */
	public void sendEmail(String recipient, String subject, String contents) throws AddressException, MessagingException {
		mailMessage = new MimeMessage(mailSession);
		mailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
		
		// Set subject
		mailMessage.setSubject(subject);
				
		// Set content
		mailMessage.setContent(contents, "text/html");
		
		
		transport = mailSession.getTransport("smtp");
		transport.connect("smtp.gmail.com", this.email, this.password);
		transport.sendMessage(mailMessage, mailMessage.getAllRecipients());
		transport.close();
		System.out.println("Email sent successfully!");
	}
}