import java.util.Calendar;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;

public class Spider implements Runnable {
	
	// Fields
	private Set<String> pagesVisited = new HashSet<String>();
	private List<String> pagesToVisit = new LinkedList<String>();
	private Target target;
	private List<Product> products = new LinkedList<Product>();
	private Crawler.CRAWLTYPE crawlType;
	private int alertInterval;
	
	/*
	 * Constructor
	 */
	public Spider(Target target, Crawler.CRAWLTYPE crawlType, int alertInterval) {
		this.target = target;
		this.crawlType = crawlType;
		this.alertInterval = alertInterval;
	}

	/*
	 * Required for multi-threading
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		this.search();
		this.reapProducts();
	}
	
	/* 
	 * Returns the next URL to visit (in order they are found).
	 * URL is also removed from pagesToVisit
	 */ 
	private String nextUrl() {
		String nextUrl;
		
		do {
			nextUrl = this.pagesToVisit.remove(0);
		} while (this.pagesVisited.contains(nextUrl));
		
		this.pagesVisited.add(nextUrl);
		return nextUrl;
	}
	
	/*
	 * Starting point of our crawler. It creates SpiderLegs that 
	 * makes a HTTP request and parses the response
	 * repeats for each searchTerm in target 
	 * 
	 * it then collects products from leg#searchForWord and filters
	 * results with #filterProducts
	 */
	public void search() {
		for (int i = 0; i < target.getBaseUrls().size(); i++) {
			String baseUrl = target.getBaseUrls().get(i);
			pagesToVisit.add(baseUrl);
			pagesVisited.add(baseUrl + "&page=1"); // prevent crawling 1st page twice
			
			while (this.pagesToVisit.size() > 0) {
				String currentUrl;
				SpiderLeg leg = new SpiderLeg();
				
				currentUrl = this.nextUrl();
				
				if (leg.crawl(currentUrl)) {
					filterProducts(leg.searchForWord(), target.getSearchTerms().get(i));
					filterLinks(leg.getLinks(), baseUrl);
				}	
			}

			System.out.println(String.format("\n**Done** Visited %s web page(s)", this.pagesVisited.size()-1));
			pagesVisited.clear();
		}
	}
	
	/*
	 * Filters out links based on the following conditions
	 * -link does not contain Crawler.ignore
	 * -link has not been visited
	 * 
	 * "&t=" time tag in URL is also removed before adding to links list
	 * 
	 * @param links - links to filter
	 * @param baseUrl - link not containing this will be filtered out
	 */
	private	void filterLinks(List<String> links, String baseUrl) {
		String simpleLink;
		
		for (String link : links) {
			// remove timestamp from url
			int tIndex = link.lastIndexOf("&t=");
			
			if (tIndex > -1) {
				simpleLink = link.substring(0, tIndex);
			} else {
				simpleLink = link;
			}
			//System.out.println("link found: " + simpleLink);
			
			if (!this.pagesVisited.contains(simpleLink) && !isIgnore(simpleLink)) {
				this.pagesToVisit.add(simpleLink);
				//System.out.println("Added >> " + simpleLink);
			} else {
				//System.out.println("Ignored >> " + simpleLink);
			}
		}
	}
	
	/*
	 * Checks if link is is ignore list
	 * 
	 * @param link - link to check
	 */
	private boolean isIgnore(String link) {
		for (String ignore : Crawler.ignoreList) {
			if (link.contains(ignore)) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Filters and adds products based on provided blacklist and searchterm
	 * additional filter on product recency is done if crawlType: NEW
	 * 
	 * @param unfilteredProducts - List of products to filter
	 * @param searchTerm - passed to #isMatch to perform matching with
	 */
	private void filterProducts(List<Product> unfilteredProducts, String searchTerm) {
		for (Product product : unfilteredProducts) {
			// check if product name contains all words in searchTerm and does not contain blacklisted words
			if (isMatch(product, searchTerm) && !isBlacklisted(product.getName()) && !isDuplicate(product)) {	
				switch (crawlType) {
				case SUMMARY:
					System.out.println("Filter style: ALL");
					System.out.println("Found: " + product.toString(false) +"\n");
					products.add(product);	
					break;
				case ALERT:		// filter further by recency of product
					if (product.getAgeInSeconds() <= this.alertInterval/1000) {
						System.out.println("Filter style: NEW");
						System.out.println("Found: " + product.toString(false) +"\n");
						products.add(product);
					}
					break;
				}		
			} 
		}
	}
	
	/*
	 * Checks too see if product is a duplicate post
	 * 
	 * @param pending - Product to perform duplication check
	 * @return - true if duplicate, false otherwise
	 */
	private boolean isDuplicate(Product pending) {
		for (Product product : products) {
			if (product.getLink().equals(pending.getLink()) || product.toString(false).equals(pending.toString(false))) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Checks if product name contains all the words in search term
	 * and product ageValue is lower or equal to target ageLimit
	 * search term is split by spaces into individual words before matching
	 * 
	 * @param product - product found to perform match
	 * @param searchTerm - searchTerm to perform match with
	 * @return - returns false if any words are not found in product name
	 */
	private boolean isMatch(Product product, String searchTerm) {
		//ageValue check
		int ageLimit = target.getAgeLimit();
		if (product.getAgeInSeconds() > ageLimit) {
			return false;
		}
		
		//name check
		String[] indiWords = searchTerm.split("\\s+");
		for (String word : indiWords) {
			if (!product.getName().toLowerCase().contains(word.toLowerCase())) {
				return false;
			}
		}
		return true;
	}
	
	/*
	 * Checks to see if product name contains words in the blacklist
	 * 
	 * @return - returns true if blacklisted, false otherwise
	 */
	private boolean isBlacklisted(String productName) {
		for (String word : target.getBlackList()) {
			if (productName.toLowerCase().contains(word.toLowerCase())) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Stores all the matching products found in results table
	 * Results are sorted by price
	 */
	public void reapProducts() {	
		Calendar calendar = Calendar.getInstance(); // Time stamp should be stored in results table?
		int count = products.size();
		
		//sort products by price
		Collections.sort(this.products);
		
		System.out.println("\n=== " + count +" products found ===");
		for (Product product : products) {
			boolean status = SQLManager.storeResults(target.getID(), product.getName(), product.getPrice(), product.getSeller(), product.getAge(), product.getLink());
			if (status == false) { System.err.println("reapProducts() ERROR: failed to insert result:" + product.getName()); }
		}
		
		for (Product product : products) {
			System.out.println(product.toString(false) + "\n");
		}
		
	}

}
