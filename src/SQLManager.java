import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class SQLManager {
	private static final String user = ConfigReader.getPropValue("databaseuser");
	private static final String password = ConfigReader.getPropValue("databasepass");
	private Connection connect = null;
	
	
	public static void main(String args[]) throws FileNotFoundException, SQLException {
		txtToSQL();
	}
	
	/*
	 * TEMPORARY: parses carousell category data from input.txt to MySQL Database
	 */
	private static void txtToSQL() throws FileNotFoundException, SQLException {
		SQLManager sqlm = new SQLManager();
		sqlm.init("local");
		
		FileReader fr = new FileReader("input.txt");
		Scanner scn = new Scanner(fr);
		
		boolean isParent;
		int current_id = 400;
		int parent_id = 0;
		
		while (scn.hasNextLine()) {
			String str = scn.nextLine();
			if (str.trim().isEmpty()) {
				System.out.println();
				continue;
			}
			
			// identifies sub and parent categories
			if (!Character.isWhitespace(str.charAt(0))) {
				isParent = true;
			} else {
				isParent = false;
			}
			
			// parse collection name and id
			String name = str.substring(0, str.indexOf(':')).trim();
			int id = Integer.parseInt(str.substring(str.indexOf(':')+2).trim());
			
			String padding;
			if (isParent) {
				current_id++;
				parent_id = current_id;
				padding = "";
				// insert into database
				Statement statement =  sqlm.connect.createStatement();
				statement.executeUpdate("INSERT INTO categories (name, collection_id)"
									    + " VALUES " + "(\"" + name + "\"," + id +")");
			} else {
				current_id++;
				padding = "   ";
				// insert into database
				Statement statement =  sqlm.connect.createStatement();
				statement.executeUpdate("INSERT INTO categories (name, collection_id, parent_id)"
									    + " VALUES " + "(\"" + name + "\"," + id + "," + parent_id + ")");
			}
			System.out.println(padding + "Cater: " + name + " / " + "ID: " + id + " Parent: " + parent_id) ;	
		}
		
		sqlm.terminate();
	}

	/*
	 * Initiates connection to DB
	 */
	public void init(String SQLserver) {
		try {
			// Loads MySQL driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup connection with DB
			if (SQLserver.equals("local")) {
				this.connect = DriverManager.getConnection("jdbc:mysql://localhost/crawler?user=" + user + "&password=" + password + "&useSSL=false");
			} else if (SQLserver.equals("rds")) {
				this.connect = DriverManager.getConnection("jdbc:mysql://sohzx2000.cld2vppa6xgc.ap-southeast-1.rds.amazonaws.com/crawler?user=" + user + "&password=" + password + "&useSSL=false");
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println("SQLManager.init() ERROR: error establishing connection with database");
			e.printStackTrace();
		}
	}
	
	/*
	 * Closes connection to DB
	 */
	public void terminate() {
		try {
			this.connect.close();
		} catch (SQLException e) {
			System.err.println("SQLManager.terminate() ERROR: failed to close DB Connection");
			e.printStackTrace();
		}
	}
	
	/*
	 * gets the ID of a user from provided username
	 * 
	 * @param username - username to query user_id from
	 */
	public static int getUserID(String username) {
		SQLManager sqlm = new SQLManager();
		sqlm.init("local");
		
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = sqlm.connect.prepareStatement("SELECT * from crawler.users WHERE binary name = ?");
			preparedStatement.setString(1, username);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				return resultSet.getInt("id");
			} else {
				return -1;
			}
		} catch (SQLException e) {
			System.err.println("SQLManager.getUserID() ERROR: failed to get id for:" + username);
			e.printStackTrace();
			return -1;
		} finally {
			sqlm.terminate();
			try { if (resultSet != null ) resultSet.close(); } catch (SQLException e) { System.err.println("getCategoryName() ERROR: failed to close ResultSet"); }
			try { if (preparedStatement != null ) preparedStatement.close(); } catch (SQLException e) { System.err.println("getCategoryName() ERROR: failed to close Statement"); }
		}
	}
	
	/*
	 * gets the users preferred crawl intervals
	 * 
	 * @param userID - primary key
	 */
	public static int getUserInterval(int userID, Crawler.CRAWLTYPE crawlType) {
		SQLManager sqlm = new SQLManager();
		sqlm.init("local");
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = sqlm.connect.prepareStatement("SELECT * from crawler.users WHERE id = ?");
			preparedStatement.setInt(1, userID);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			if (crawlType == Crawler.CRAWLTYPE.SUMMARY) return resultSet.getInt("summary_interval");
			if (crawlType == Crawler.CRAWLTYPE.ALERT) return resultSet.getInt("alert_interval");
		} catch (SQLException e) {
			System.err.println("SQLManager.getUserEmail() ERROR: failed to get email for id :" + userID);
			e.printStackTrace();
		} finally {
			sqlm.terminate();
			try { if (resultSet != null ) resultSet.close(); } catch (SQLException e) { System.err.println("getCategoryName() ERROR: failed to close ResultSet"); }
			try { if (preparedStatement != null ) preparedStatement.close(); } catch (SQLException e) { System.err.println("getCategoryName() ERROR: failed to close Statement"); }
		}
		
		return 0; // error getting crawl interval 
	}
	
	/*
	 * gets the email of a user from provided userID
	 * 
	 * @param userID - primary key to query email with
	 */
	public static String getUserEmail(int userID) {
		SQLManager sqlm = new SQLManager();
		sqlm.init("local");
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = sqlm.connect.prepareStatement("SELECT * from crawler.users WHERE id = ?");
			preparedStatement.setInt(1, userID);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getString("email");
		} catch (SQLException e) {
			System.err.println("SQLManager.getUserEmail() ERROR: failed to get email for id :" + userID);
			e.printStackTrace();
			return "";
		} finally {
			sqlm.terminate();
			try { if (resultSet != null ) resultSet.close(); } catch (SQLException e) { System.err.println("getCategoryName() ERROR: failed to close ResultSet"); }
			try { if (preparedStatement != null ) preparedStatement.close(); } catch (SQLException e) { System.err.println("getCategoryName() ERROR: failed to close Statement"); }
		}
	}
	
	/*
	 * returns a list of all IDs in the groups table
	 * 
	 * @param userID - foreign key to filter groups to only those belonging to current user
	 * @return - all group IDs found
	 */
	public static List<Integer> getGroupIDs(int userID) {
		SQLManager sqlm = new SQLManager();
		sqlm.init("local");
		
		List<Integer> groupIDs = new LinkedList<Integer>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = sqlm.connect.prepareStatement("SELECT * from crawler.groups WHERE user_id = ?");
			preparedStatement.setInt(1, userID);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				groupIDs.add(resultSet.getInt("id"));
			}
		} catch (SQLException e) {
			System.err.println("SQLManager.getGroupIDs ERROR: failed to get IDs from groups table");
			e.printStackTrace();
		} finally {
			try { if (resultSet != null ) resultSet.close(); } catch (SQLException e) { System.err.println("getGroupIDs() ERROR: failed to close ResultSet"); }
			try { if (preparedStatement != null ) preparedStatement.close(); } catch (SQLException e) { System.err.println("getGroupIDs() ERROR: failed to close Statement"); }
		}
		
		sqlm.terminate();
		return groupIDs;
	}
	
	/*
	 * gets the name of a group from provided ID
	 * 
	 * @param groupID - ID to query group name from
	 */
	public static String getGroupName(int groupID) {
		SQLManager sqlm = new SQLManager();
		sqlm.init("local");
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = sqlm.connect.prepareStatement("SELECT * from crawler.groups WHERE id = ?");
			preparedStatement.setInt(1, groupID);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getString("name");
		} catch (SQLException e) {
			System.err.println("SQLManager.getGroupName() ERROR: failed to get id:" + groupID);
			e.printStackTrace();
			return "";
		} finally {
			sqlm.terminate();
			try { if (resultSet != null ) resultSet.close(); } catch (SQLException e) { System.err.println("getGroupName() ERROR: failed to close ResultSet"); }
			try { if (preparedStatement != null ) preparedStatement.close(); } catch (SQLException e) { System.err.println("getGroupName() ERROR: failed to close Statement"); }
		}
	}
	
	/*
	 * Gets all targets of a specified group
	 * 
	 * @param groupID - ID of group to get targets for
	 * @return - list of target instances
	 */
	public static List<Target> getTargets(int groupID) {
		SQLManager sqlm = new SQLManager();
		sqlm.init("local");
		
		List<Target> targets = new LinkedList<Target>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = sqlm.connect.prepareStatement("SELECT * FROM crawler.targets INNER JOIN crawler.categories ON targets.category_id = categories.id where group_id = ?");
			preparedStatement.setInt(1, groupID);
			resultSet = preparedStatement.executeQuery();
			
			// build target
			while (resultSet.next()) {
				Target target;
				int targetID = resultSet.getInt("id");
				String name = resultSet.getString("name");
				int collectionID = resultSet.getInt("collection_id");
				String terms = resultSet.getString("terms");
				String blacklist = resultSet.getString("blacklist");
				int minPrice = resultSet.getInt("min_price");
				int maxPrice = resultSet.getInt("max_price");
				String maxAge = resultSet.getString("max_age");
				
				target = new Target(targetID, name, collectionID, terms, blacklist, minPrice, maxPrice, maxAge); // extract data from current resultSet row to construct Target instance
				targets.add(target);
			}
		} catch (SQLException e) {
			System.err.println("SQLManager.getTargets() ERROR: failed to build targets from targets table");
			e.printStackTrace();
		} finally {
			try { if (resultSet != null ) resultSet.close(); } catch (SQLException e) { System.err.println("getTargets() ERROR: failed to close ResultSet"); }
			try { if (preparedStatement != null ) preparedStatement.close(); } catch (SQLException e) { System.err.println("getTargets() ERROR: failed to close Statement"); }
		}
		
		sqlm.terminate();
		return targets;
	}
	
	/*
	 * Gets all active results (not yet mailed) of a specified target mapped as Product instances
	 * 
	 * @param targetID - ID of target row to get results for
	 * @return - list of Product instances
	 */
	public static List<Product> getActiveResults(int targetID) {
		SQLManager sqlm = new SQLManager();
		sqlm.init("local");
		
		List<Product> results = new LinkedList<Product>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = sqlm.connect.prepareStatement("SELECT * FROM crawler.results WHERE target_id = ? AND is_active = true");
			preparedStatement.setInt(1, targetID);
			resultSet = preparedStatement.executeQuery();
			
			// build product
			while (resultSet.next()) {
				Product result;
				String name = resultSet.getString("name");
				String price =  "$" + resultSet.getDouble("price"); // Product constructor takes price as a string
				String link = resultSet.getString("link");
				String seller = resultSet.getString("seller");
				String age = resultSet.getString("age");
				result = new Product(name, price, link, seller, age); // extract data from current resultSet row to construct Target instance
				results.add(result);
			}
		} catch (SQLException e) {
			System.err.println("SQLManager.getTargets() ERROR: failed to build targets from targets table");
			e.printStackTrace();
		} finally {
			try { if (resultSet != null ) resultSet.close(); } catch (SQLException e) { System.err.println("getResults() ERROR: failed to close ResultSet"); }
			try { if (preparedStatement != null ) preparedStatement.close(); } catch (SQLException e) { System.err.println("getResults() ERROR: failed to close Statement"); }
		}
		
		sqlm.terminate();
		return results;
	}
	
	/*
	 * Writes results into results table in DB
	 * 
	 * @param - parameters corresponding to each column in results table
	 * @return - true/false depending on success or failure of SQL insert
	 */
	public static boolean storeResults(int targetID, String name, double price, String seller, String age, String link) {
		SQLManager sqlm = new SQLManager();
		sqlm.init("local");
		
		boolean status = true;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = sqlm.connect.prepareStatement("INSERT INTO crawler.results VALUES (default, ?, ?, ?, ?, ?, ?, ?, now(), now())");
			preparedStatement.setString(1, name);
			preparedStatement.setDouble(2, price);
			preparedStatement.setString(3, seller);
			preparedStatement.setString(4, age);
			preparedStatement.setInt(5, targetID);
			preparedStatement.setString(6, link);
			preparedStatement.setBoolean(7, true);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("storeResults() ERROR: failed to insert result into results table"); 
			e.printStackTrace();
			status = false;
		} finally {
			try { if (preparedStatement != null ) preparedStatement.close(); } catch (SQLException e) { System.err.println("storeResults() ERROR: failed to close PreparedStatement"); }
		}
		
		sqlm.terminate();
		return status;
	}
	
	/*
	 * sets is_active of selected rows in results table to false
	 */
	public static void resetResults(List<Integer> groupIDs) {
		SQLManager sqlm = new SQLManager();
		sqlm.init("local");
		
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = sqlm.connect.prepareStatement("UPDATE results INNER JOIN targets ON targets.id = results.target_id SET is_active = false WHERE targets.group_id = ?");
			for (Integer ID : groupIDs) {
				preparedStatement.setInt(1, ID);
				preparedStatement.executeUpdate();
				System.out.println("result reset done for groupID: " + ID);
			}
			System.out.println("Results table successfully reset.");
		} catch (SQLException e) {
			System.err.println("resetResults() ERROR: failed to reset results"); 
			e.printStackTrace();
		} finally {
			try { if (preparedStatement != null ) preparedStatement.close(); } catch (SQLException e) { System.err.println("clearResults() ERROR: failed to close Statement"); }
		}
		
		sqlm.terminate();
	}
}
