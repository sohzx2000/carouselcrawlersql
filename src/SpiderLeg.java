import java.util.List;
import java.util.Set;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SpiderLeg {
	
	// Fields
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";
	private List<String> links = new LinkedList<String>();
	private Document htmlDocument;
	
	/*
	 * Makes HTTP request, checks response,
	 * gathers all the links on page.
	 * 
	 * @param url - the URL to visit
	 * @return - whether the crawl was successful
	 */
	public boolean crawl(String url) {
		try {
			Connection connection = Jsoup.connect(url).userAgent(USER_AGENT).timeout(5*1000);
			Document htmlDocument = connection.get();
			this.htmlDocument = htmlDocument;
			
			if (connection.response().statusCode() == 200) {
				// HTTP OK
				System.out.println("\n**Visiting** Received web page at " + url);
			}
			
			if (!connection.response().contentType().contains("text/html")) {
				// page is not HTML
				System.out.println("**Failure** Retrieved something other than HTML");
				return false;
			}
			
			// collect all the links [modified to get next page links]
			Elements linksOnPage = htmlDocument.select("li.pagination-next a[href]");
			for (Element link : linksOnPage) {
				links.add(link.absUrl("href"));
			}
			return true;
			
		} catch (IOException e) {
			// unsuccessful in our HTTP request
			System.out.println("Error in out HTTP request" + e);
			return false;
		}
	}
	
	/*
	 * Performs search on the body of the HTML document retrieved.
	 * This method should only be called after a successful crawl.
	 *
	 * @return - all products on page
	 */
	public List<Product> searchForWord() {
		if (this.htmlDocument == null) {
			System.out.println("ERROR! Call crawl() before performing analysis on the document");
			return null;
		}
		
		List<Product> products = new LinkedList<Product>();
		System.out.println("Obtaining all product cards...\n");
		Elements productCards = htmlDocument.select("figure.card");
		
		for (Element productCard : productCards) {
			String productName = productCard.select("h4#productCardTitle").text();
			String productLink = productCard.select("a#productCardThumbnail").attr("abs:href");
			String productPrice;
			try {
				productPrice = productCard.select("dd").first().text();
			} catch (NullPointerException e) {
				continue;
			}
			String productSeller = productCard.select("h3.media-heading").text();
			String productAge = productCard.select("time span").text();
					
			Product newProduct = new Product(productName, productPrice, productLink, productSeller, productAge);
			products.add(newProduct);				
		}
		return products;
	}
	
	public List<String> getLinks() {
		return this.links;
	}
	
}
