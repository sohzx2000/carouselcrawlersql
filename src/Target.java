import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;

public class Target {
	
	// Attributes
	private int id;
	private String name;
	private int collectionID;
	private List<String> baseUrls = new LinkedList<String>();		// one baseUrl for each searchTerm
	private List<String> blackList = new LinkedList<String>();
	private List<String> searchTerms = new LinkedList<String>();	// each Target can have multiple searchTerm
	private int priceMin;
	private int priceMax;
	private int ageInSeconds;
	
	/*
	 * Constructor
	 * Initializes attributes with columns from a target in DB
	 * 
	 * @param - variables to get attribute values from
	 */
	public Target(int targetID, String name, int collectionID, String terms, String blacklist, int minPrice, int maxPrice, String maxAge) {	
		// store DB ID
		this.id = targetID;
		
		// collectionID
		this.collectionID = collectionID;
		
		// name
		this.name = name;
		
		// list of search terms
		String[] searchTerms = terms.split("/");
		for (String term : searchTerms) {
			this.searchTerms.add(term);
		}
	
		// list of blacklisted words
		if (blacklist.length() != 0) {
			String[] words = blacklist.split(",");
			for (String word : words) {
				this.blackList.add(word);
			}
		}
		
		// minimum price
		this.priceMin = minPrice;
	
		// maximum price
		this.priceMax = maxPrice;
		
		// maximum age in seconds
		this.ageInSeconds = Tool.ageToSeconds(maxAge);
		
		formUrl();
	}
	
	/*
	 * Forms baseUrls to search from based on attributes
	 * searchWord, priceMin, priceMax
	 */
	private void formUrl() {
		for (String searchTerm : searchTerms) {
			String newUrl = "";
			
			// need to encode spaces in searchWord to "+"
			String encodedSearchWord = null;
			try {
			    encodedSearchWord = URLEncoder.encode(searchTerm, "UTF-8").replaceAll("\\+", "%20");
			} catch (UnsupportedEncodingException ignored) {
			    // Can be safely ignored because UTF-8 is always supported
			}
			
			// crawled sites must start with the baseURL
			newUrl = "https://sg.carousell.com/search/products?query=" + encodedSearchWord + "&collection_id=" + this.collectionID; 
			// piggyback on Carousell search to sort by price
			// newUrl += "&sort_by=lowest_price";
			newUrl += "&sort_by=";
			
			// add min and max price query to baseUrl if provided
			if (this.priceMin >= 0) {
				newUrl += "&price_start=" + this.priceMin;
			}
			if (this.priceMax >= 0) {
				newUrl += "&price_end=" + this.priceMax;
			}
			
			this.baseUrls.add(newUrl);
		}
	}
	
	public int getID() {
		return this.id;
	}
 	
	public String getName() {
		return this.name;
	}
	
	public List<String> getBaseUrls() {
		return this.baseUrls;
	}
	
	public List<String> getBlackList() {
		return this.blackList;
	}
	
	public List<String> getSearchTerms() {
		return this.searchTerms;
	}
	
	public int getAgeLimit() {
		return this.ageInSeconds;
	}

	public int getCollectionID() {
		return this.collectionID;
	}
}
