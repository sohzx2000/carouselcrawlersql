import java.util.Scanner;
import java.util.Timer;

public class Scheduler {
	// Implement: Get timer intervals from database
	public static int summaryInterval;
	public static int alertInterval;
	
	/*
	 * Sets up a timer that calls Crawler#run() at fixed intervals
	 *
	 * @param args - not used
	 */
	public static void main(String[] args) throws InterruptedException {
		// Verify username, userID of -1 signifies user is unable to enter valid username and has quit the program.
		int userID = getUser();
		if (userID == -1) { 
			System.out.println("Quitting Program...");
			return; 
		}
		
		// Get user's preferred crawl intervals
		summaryInterval = SQLManager.getUserInterval(userID,Crawler.CRAWLTYPE.SUMMARY);
		alertInterval = SQLManager.getUserInterval(userID, Crawler.CRAWLTYPE.ALERT);
		
		// If either interval is 0, terminate
		if (summaryInterval == 0 || alertInterval == 0) {
			System.out.println("Invalid interval parameter for user with ID: " + userID + ", crawl terminated");
			return;
		}
		
		boolean reset = true;
		Timer timer = null;
		Crawler crawlTaskSummary;
		Crawler crawlTaskAlert;
		
		while (true) {
			if (reset) {
				// Implement timer to control crawl interval
				timer = new Timer(); 
				crawlTaskSummary = new Crawler(userID, Crawler.CRAWLTYPE.SUMMARY);
				crawlTaskAlert = new Crawler(userID, Crawler.CRAWLTYPE.ALERT, alertInterval);
			
				timer.scheduleAtFixedRate(crawlTaskSummary, 0, summaryInterval);
				timer.schedule(crawlTaskAlert, 0, alertInterval);
				reset = false;
			}
		
			/*
			// Detect changes in user's interval preferences
			Thread.sleep(10000);
			System.out.println("Checking DB for changes to interval preferences...");
			
			int summaryIntervalDB = SQLManager.getUserInterval(userID, Crawler.CRAWLTYPE.SUMMARY);
			int alertIntervalDB = SQLManager.getUserInterval(userID, Crawler.CRAWLTYPE.ALERT);
			
			if (summaryIntervalDB != summaryInterval || alertIntervalDB != alertInterval) {
				System.out.println("Change in crawling interval detected, restarting threads...");
				// terminate and restart thread
				summaryInterval = summaryIntervalDB;
				alertInterval = alertIntervalDB;
				timer.cancel();
				reset = true;
			}
			*/
		}
	}
	
	/*
	 * Gets the user that is using the crawler.
	 * This is required to isolate target queries to those belonging to the user
	 */
	public static int getUser() {
		int userID;
		Scanner scn = new Scanner(System.in);
		
		while (true) {
			System.out.print("Please enter your username > ");
			String name;
			name = scn.nextLine();
			
			userID = SQLManager.getUserID(name); 
			if (userID != -1) {
				break;
			} else {
				if (Tool.isTrue("Invalid user, try again?")) {
					continue;
				} else {
					break;
				}
			}
			
		}
		return userID;
	}
}
